#!/bin/python3
import os
import sys
import csv

root_dir=os.getcwd()

with open('pointers.tsv', 'r') as csv_in :
    reader = csv.DictReader(csv_in, delimiter='\t')
    for row_in in reader :
        pointer = row_in['pointer']
        pointer_name = os.path.basename(pointer)
        pointer_subdir = os.path.dirname(pointer)
        target = row_in['target']
        pointer_absdir = os.path.join(root_dir, pointer_subdir)
        os.makedirs(pointer_absdir, exist_ok=True)
        os.chdir(pointer_absdir)
        if os.path.exists(pointer_name) :
            os.remove(pointer_name)
        os.symlink(target, pointer_name, target_is_directory = os.path.isdir(target), dir_fd = None)