import os
import csv
from win32com.client import Dispatch

data_file = 'pointers.tsv'

if not os.path.exists(data_file) :
    print(f"\nThe file {data_file} is missing. It needs to be in the same folder as this script.\nExiting.")
    exit()

def make_choice() :
    return input("\nWhich type of pointers do you want to create?\n1: Shortcuts\n2: Symbolic links\nAnswer 1 or 2: ")

choice = make_choice()
pointer_type = False
while not pointer_type :
    if choice == '1' :
        pointer_type = 'shortcuts'
    elif choice == '2' :
        pointer_type = 'symlinks'
    else :
        pointer_type = False
        print("\n\nIncorrect answer. Please Answer 1 or 2.")
        choice = make_choice()

root_dir=os.getcwd()
print("\nRunning, please wait...")

with open(data_file, 'r') as csv_in :
    reader = csv.DictReader(csv_in, delimiter='\t')
    for row_in in reader :
        pointer = row_in['pointer']
        pointer_name = os.path.basename(pointer)
        pointer_subdir = os.path.dirname(pointer)
        target = row_in['target'].replace('/','\\')
        pointer_absdir = os.path.join(root_dir, pointer_subdir)
        os.makedirs(pointer_absdir, exist_ok=True)
        os.chdir(pointer_absdir)
        if pointer_type == 'symlinks' :
            try:
                os.remove(pointer_name)
            except OSError:
                pass
            try :
                os.symlink(target, pointer_name, target_is_directory = os.path.isdir(target), dir_fd = None)
            except OSError :
                print ("\nHold on a second!\n\n",
                "For symlinks, you need to either enable Developer mode on your Windows machine or run the command prompt as an administrator.\n",
                "Although in my attempts, only administrator mode worked.\n",
                "Note that this is not required for shortcuts.\n",
                "More info:\n",
                "https://blogs.windows.com/windowsdeveloper/2016/12/02/symlinks-windows-10/")
                exit()

        elif pointer_type == 'shortcuts' :
            pointer_name = pointer_name + '.lnk'
            try:
                os.remove(pointer_name)
            except OSError:
                pass
            abs_target = os.path.abspath(target)
            shell = Dispatch('WScript.Shell')
            shortcut = shell.CreateShortCut(pointer_name)
            shortcut.Targetpath = os.path.abspath(abs_target)
            shortcut.save()

if pointer_type == 'symlinks' :
    print("\nSuccess!\nSymlinks created.")
elif pointer_type == 'shortcuts' :
    print("\nSuccess!\nShortcuts created.")
