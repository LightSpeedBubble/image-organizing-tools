# File pointers management

## Create file shortcuts from a list file


### For Windows:

1. Download [windows_pointers_from_list.py](windows_pointers_from_list.py)
2. Put it in the same folder as the "pointers.tsv" list file
3. Open a command prompt in the same folder or `cd` to it
    - (Don't run the script from your home directory or any other, you must cd to the script)
4. make sure you have the necessary python library installed by running:
    - `pip install pywin32`
5. Run the following command:
    - `python windows_pointers_from_list.py`
- You will be provided a choice between shortcuts or symlinks. Note that symlinks don't generate image thumbnails and require a special privilege to be created (running the command prompt as admin or developer mode), which isn't the case for shortcuts.
### For Linux:
1. Download [linux_symlinks_from_list.py](linux_symlinks_from_list.py)
2. Put it in the same folder as the "pointers.tsv" list file
3. Open a terminal in the same directory
    - (Don't run the script from your home directory or any other, you must cd to the script)
4. Run:
    - `./linux_symlinks_from_list.py`

### **Side note:**
On Windows, Image viewers don't cycle through shortcuts or symlinks that point to different folders. Instead, they jump to the target folder and cycle though the images that are there.

On Linux, image viewers stay in the folder you opened them from and cycle through symlinks regardless of where they point to (bonus point!).

### **If you don't already have a list file**
... and want to create one, your list file must:
- be named pointers.tsv
- be located in the same folder as the script
- have a tabulation delimiter
- contain 2 columns with headers named "pointer" and "target" :
  - pointer is a relative path from the list file's location defining where a pointer will be created
  - target is a relative path from the pointer's location defining where the pointer will point to.

Example :
| pointer | target |
|-|-|
| ./pointers/img1.jpg | ../../files/img1.jpg |
| ./pointers/img2.jpg | ../../files/img2.jpg |
