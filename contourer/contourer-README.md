# Contourer:
Generates a copy of a folder (including subfolders) in which all images are filtered to have only the contour lines with added thickness, in order for image similarity finders like [Dupeguru](https://dupeguru.voltaicideas.net/) to find a higher similarity between drawings at different stages :

![](contourer/contourer-example1.jpg)
![](contourer/contourer-example2.jpg)

[CC BY](https://creativecommons.org/licenses/by/4.0/) Art by David Revoy, ([image 1](https://www.davidrevoy.com/article609/merci), [image 2](https://www.davidrevoy.com/article772/mysterious))

## Interface:

![](contourer/contourer-preview.png)


## Installation:
1. Install the dependencies:
  - yad
  - zenity
  - gmic
  - parallel
  - libglib2.0-bin (for `gio trash` command)
2. Paste the contourer script in your file browser's dedicated folder for context menu scripts (e.g. ~/.local/share/nautilus/scripts). Works with other file browsers like Nemo or Caja. For Dolphin, an additional desktop file has to be created (feel free to ask me, I can add it to the repo).
3. Make sure the script is executable.

## Usage:
Right click on any folder, select the script, enter your choices and it will create a copy of the folder with "-contours" added to the name and generate all the filtered images inside. If you chose the "update" option, it will only make the necessary changes.

## Explanation:
The fundamental command of this script is a gmic sequence :

`gmic "path/to/my/image.jpg" +resize_ratio2d 1000,1000,1,3 -edges 10 -n 0,255 -erode_circ 5,1 -n 0,255 -resize_as_image [0],3 -remove[0] -o "path/to/the/copy.jpg"`

To explain what the sequence does :
1. resize the image to be 1000px max in either width or height. This is because when we dilate the contours, we want the thickness to look the same even if the sketch drawing is super small and the finished drawing is super large, so we temporarily conform all images to the same size.
2. apply an "edge" filter with a threshold of 10. The threshold is a % value between 0 and 100. The lower the value, the more contour lines are drawn.
3. dilate the lines ("erode" for gmic because the lines are black) by 5 pixels
4. resize the image back to its original size. This ensures that the image dimension values displayed in the dupe finder (e.g. Dupeguru) are correct.
